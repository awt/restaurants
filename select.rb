require 'csv'
require 'set'
require 'pp'

prices = File.read(ARGV[0])
items = ARGV[1 .. -1]

menus = {}
priced_menus = {}

csv = CSV.parse(prices)
csv.each do |row|
    menus[row[0]] = [] if !menus[row[0]]
    priced_menus[row[0]] = [] if !priced_menus[row[0]]

    menu = menus[row[0]]
    priced_menu = priced_menus[row[0]]
    if row.size < 4
      menu.push(row[2].strip)
    else 
      menu.push(row[2 .. -1].collect{|i|i.strip})
    end
    priced_menu.push(row[1 .. -1].collect{|i|i.strip})
end

candidates = []
menus.each_pair do |k, v|
  if v.flatten.to_set.superset? items.to_set
    candidates.push(k)
  end
end

#source: http://stackoverflow.com/questions/2037327/translating-function-for-finding-all-partitions-of-a-set-from-python-to-ruby
def partitions(set)
  yield [] if set.empty?
  (0 ... 2 ** set.size / 2).each do |i|
    parts = [[], []]
    set.each do |item|
      parts[i & 1] << item
      i >>= 1
    end
    partitions(parts[1]) do |b|
      result = [parts[0]] + b
      result = result.reject do |e|
        e.empty?
      end
      yield result
    end
  end
end

def price(matched_item_indices)
  prices = {}
  matched_item_indices.each_value do |values|
    prices[values[0]] = values[1]
  end
  total = 0
  prices.each_value do |value|
    total += value.to_f
  end
  return total
end

def menu_items(matched_item_indices)
  menu_items = Set.new
  matched_item_indices.each_value do |values|
    menu_items.add(values[0])
  end
  return menu_items.to_a
end

def partition_matches?(menu, partition)
  matched_item_indices = {}
  found_match_for_partition = true
  partition.each do |p|
    found_match_for_grouping = false
    menu.each_with_index do |item, i|
      price = item[0]
      if p.to_set.subset? item[1 .. -1].to_set
        #there may be multiple matches for a particular grouping so we're naivly choosing the least expensive match.  This may be an issue.
        if matched_item_indices[p].nil? || price.to_f < matched_item_indices[p][1].to_f
          matched_item_indices[p] = [i, price] 
        end
        found_match_for_grouping = true
      end
    end
    found_match_for_partition = false if !found_match_for_grouping
  end
  return found_match_for_partition ? matched_item_indices : nil
end

results = []
candidates.each do |candidate|
  menu = priced_menus[candidate]
  partitions(items) do |partition|
    
    #search each menu item for a match for each grouping
    
    matches = partition_matches?(menu, partition)
    results.push([candidate, menu_items(matches), price(matches)]) if matches
  end
end


best_deal = results.sort{|a,b| a[2] <=> b[2]}.first
if(best_deal)
  puts "#{best_deal[0]}, #{best_deal[-1]}"
else
  puts 'nil'
end

