To execute this program, enter:
ruby select.rb <csv-file> <item1> <item2> ... <itemN>

Assumptions:
Value meals include only one of any type of item.

There are no duplicate value meals with the different prices for a given
restaurant.

One meal with the desired items for a given price is as good as
another.

Approach:
Generate as many potential pricings of a meal as possible and sort the
results for the one with the lowest price.
